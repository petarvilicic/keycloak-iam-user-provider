FROM jboss/keycloak:3.4.2.Final

MAINTAINER petar.vilicic@intis.hr

RUN mkdir -p $JBOSS_HOME/modules/com/intis/keycloak/iam/user/provider/main

ADD build/libs/keycloak-iam-user-provider-1.0.jar $JBOSS_HOME/modules/com/intis/keycloak/iam/user/provider/main
ADD keycloak_config/module.xml $JBOSS_HOME/modules/com/intis/keycloak/iam/user/provider/main

COPY keycloak_config/standalone.xml $JBOSS_HOME/standalone/configuration
COPY keycloak_config/themes $JBOSS_HOME/themes
