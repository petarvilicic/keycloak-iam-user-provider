package com.intis.keycloak.iam.user.provider;

import org.keycloak.models.*;
import org.keycloak.models.jpa.JpaUserProvider;
import org.keycloak.models.jpa.UserAdapter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class IamUserProvider extends JpaUserProvider {

    private KeycloakSession session;

    public IamUserProvider(KeycloakSession session, EntityManager em) {
        super(session, em);
        this.session = session;
    }

    @Override
    public UserModel addUser(RealmModel realm, String id, String username, boolean addDefaultRoles, boolean addDefaultRequiredActions) {
        UserAdapter userAdapter = (UserAdapter) super.addUser(realm, id, username, addDefaultRoles, addDefaultRequiredActions);
        if (userAdapter == null) {
            return null;
        }
        return new IamUserAdapter(userAdapter, session, realm, em, userAdapter.getEntity());
    }

    @Override
    public UserModel getUserById(String id, RealmModel realm) {
        UserAdapter userAdapter = (UserAdapter) super.getUserById(id, realm);
        if (userAdapter == null) {
            return null;
        }
        return new IamUserAdapter(userAdapter, session, realm, em, userAdapter.getEntity());
    }

    @Override
    public UserModel getUserByUsername(String username, RealmModel realm) {
        UserAdapter userAdapter = (UserAdapter) super.getUserByUsername(username, realm);
        if (userAdapter == null) {
            return null;
        }
        return new IamUserAdapter(userAdapter, session, realm, em, userAdapter.getEntity());
    }

    @Override
    public UserModel getUserByEmail(String email, RealmModel realm) {
        UserAdapter userAdapter = (UserAdapter) super.getUserByEmail(email, realm);
        if (userAdapter == null) {
            return null;
        }
        return new IamUserAdapter(userAdapter, session, realm, em, userAdapter.getEntity());
    }

    @Override
    public UserModel getUserByFederatedIdentity(FederatedIdentityModel identity, RealmModel realm) {
        UserAdapter userAdapter = (UserAdapter) super.getUserByFederatedIdentity(identity, realm);
        if (userAdapter == null) {
            return null;
        }
        return new IamUserAdapter(userAdapter, session, realm, em, userAdapter.getEntity());
    }

    @Override
    public UserModel getServiceAccount(ClientModel client) {
        UserAdapter userAdapter = (UserAdapter) super.getServiceAccount(client);
        if (userAdapter == null) {
            return null;
        }
        return new IamUserAdapter(userAdapter, session, client.getRealm(), em, userAdapter.getEntity());
    }

    @Override
    public List<UserModel> getUsers(RealmModel realm, int firstResult, int maxResults, boolean includeServiceAccounts) {
        List<UserModel> userAdapters = super.getUsers(realm, firstResult, maxResults, includeServiceAccounts);
        return toListOfIamUserAdapters(userAdapters, realm);
    }

    @Override
    public List<UserModel> getGroupMembers(RealmModel realm, GroupModel group, int firstResult, int maxResults) {
        List<UserModel> userAdapters = super.getGroupMembers(realm, group, firstResult, maxResults);
        return toListOfIamUserAdapters(userAdapters, realm);
    }

    @Override
    public List<UserModel> getRoleMembers(RealmModel realm, RoleModel role, int firstResult, int maxResults) {
        List<UserModel> userAdapters = super.getRoleMembers(realm, role, firstResult, maxResults);
        return toListOfIamUserAdapters(userAdapters, realm);
    }

    @Override
    public List<UserModel> searchForUser(String search, RealmModel realm, int firstResult, int maxResults) {
        List<UserModel> userAdapters = super.searchForUser(search, realm, firstResult, maxResults);
        return toListOfIamUserAdapters(userAdapters, realm);
    }

    @Override
    public List<UserModel> searchForUser(Map<String, String> attributes, RealmModel realm, int firstResult, int maxResults) {
        List<UserModel> userAdapters = super.searchForUser(attributes, realm, firstResult, maxResults);
        return toListOfIamUserAdapters(userAdapters, realm);
    }

    @Override
    public List<UserModel> getGroupMembers(RealmModel realm, GroupModel group) {
        List<UserModel> userAdapters = super.getGroupMembers(realm, group);
        return toListOfIamUserAdapters(userAdapters, realm);
    }

    @Override
    public List<UserModel> getRoleMembers(RealmModel realm, RoleModel role) {
        List<UserModel> userAdapters = super.getRoleMembers(realm, role);
        return toListOfIamUserAdapters(userAdapters, realm);
    }

    @Override
    public List<UserModel> searchForUserByUserAttribute(String attrName, String attrValue, RealmModel realm) {
        List<UserModel> userAdapters = super.searchForUserByUserAttribute(attrName, attrValue, realm);
        return toListOfIamUserAdapters(userAdapters, realm);
    }

    private List<UserModel> toListOfIamUserAdapters(List<UserModel> userAdapters, RealmModel realm) {
        return userAdapters.stream().map(e -> new IamUserAdapter((UserAdapter) e, session, realm, em, ((UserAdapter) e).getEntity())).collect(Collectors.toList());
    }

}
