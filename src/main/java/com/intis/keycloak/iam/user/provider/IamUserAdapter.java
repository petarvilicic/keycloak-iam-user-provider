package com.intis.keycloak.iam.user.provider;

import org.jboss.logging.Logger;
import org.keycloak.models.*;
import org.keycloak.models.jpa.UserAdapter;
import org.keycloak.models.jpa.entities.UserEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class IamUserAdapter implements UserModel {

    private static final Logger log = Logger.getLogger(IamUserAdapter.class);
    private final KeycloakSession session;
    private UserAdapter userAdapter;
    private UserEntity user;
    private EntityManager em;
    private RealmModel realm;

    public IamUserAdapter(UserAdapter userAdapter, KeycloakSession session, RealmModel realm, EntityManager em, UserEntity user) {
        this.userAdapter = userAdapter;
        this.em = em;
        this.user = user;
        this.realm = realm;
        this.session = session;
    }

    @Override
    public String getId() {
        return userAdapter.getId();
    }

    @Override
    public String getUsername() {
        return userAdapter.getUsername();
    }

    @Override
    public void setUsername(String username) {
        userAdapter.setUsername(username);
    }

    @Override
    public Long getCreatedTimestamp() {
        return userAdapter.getCreatedTimestamp();
    }

    @Override
    public void setCreatedTimestamp(Long timestamp) {
        userAdapter.setCreatedTimestamp(timestamp);
    }

    @Override
    public boolean isEnabled() {
        return userAdapter.isEnabled();
    }

    @Override
    public void setEnabled(boolean enabled) {
        userAdapter.setEnabled(enabled);
    }

    @Override
    public void setSingleAttribute(String name, String value) {
        userAdapter.setSingleAttribute(name, value);
    }

    @Override
    public void setAttribute(String name, List<String> values) {
        userAdapter.setAttribute(name, values);
    }

    @Override
    public void removeAttribute(String name) {
        userAdapter.removeAttribute(name);
    }

    @Override
    public String getFirstAttribute(String name) {
        return userAdapter.getFirstAttribute(name);
    }

    @Override
    public List<String> getAttribute(String name) {
        return userAdapter.getAttribute(name);
    }

    @Override
    public Map<String, List<String>> getAttributes() {
        return userAdapter.getAttributes();
    }

    @Override
    public Set<String> getRequiredActions() {
        return userAdapter.getRequiredActions();
    }

    @Override
    public void addRequiredAction(String action) {
        userAdapter.addRequiredAction(action);
    }

    @Override
    public void removeRequiredAction(String action) {
        userAdapter.removeRequiredAction(action);
    }

    @Override
    public void addRequiredAction(RequiredAction action) {
        userAdapter.addRequiredAction(action);
    }

    @Override
    public void removeRequiredAction(RequiredAction action) {
        userAdapter.removeRequiredAction(action);
    }

    @Override
    public String getFirstName() {
        return userAdapter.getFirstName();
    }

    @Override
    public void setFirstName(String firstName) {
        userAdapter.setFirstName(firstName);
    }

    @Override
    public String getLastName() {
        return userAdapter.getLastName();
    }

    @Override
    public void setLastName(String lastName) {
        userAdapter.setLastName(lastName);
    }

    @Override
    public String getEmail() {
        return userAdapter.getEmail();
    }

    @Override
    public void setEmail(String email) {
        userAdapter.setEmail(email);
    }

    @Override
    public boolean isEmailVerified() {
        return userAdapter.isEmailVerified();
    }

    @Override
    public void setEmailVerified(boolean verified) {
        userAdapter.setEmailVerified(verified);
    }

    @Override
    public Set<GroupModel> getGroups() {
        return userAdapter.getGroups();
    }

    @Override
    public void joinGroup(GroupModel group) {
        userAdapter.joinGroup(group);
    }

    @Override
    public void leaveGroup(GroupModel group) {
        userAdapter.leaveGroup(group);
    }

    @Override
    public boolean isMemberOf(GroupModel group) {
        return userAdapter.isMemberOf(group);
    }

    @Override
    public String getFederationLink() {
        return userAdapter.getFederationLink();
    }

    @Override
    public void setFederationLink(String link) {
        userAdapter.setFederationLink(link);
    }

    @Override
    public String getServiceAccountClientLink() {
        return userAdapter.getServiceAccountClientLink();
    }

    @Override
    public void setServiceAccountClientLink(String clientInternalId) {
        userAdapter.setServiceAccountClientLink(clientInternalId);
    }

    // TODO overrideati!

    @Override
    public Set<RoleModel> getRealmRoleMappings() {
        return userAdapter.getRealmRoleMappings();
    }

    @Override
    public Set<RoleModel> getClientRoleMappings(ClientModel app) {
        return userAdapter.getClientRoleMappings(app);
    }

    @Override
    public boolean hasRole(RoleModel role) {
        return userAdapter.hasRole(role);
    }

    @Override
    public void grantRole(RoleModel role) {
        userAdapter.grantRole(role);
    }

    @Override
    public Set<RoleModel> getRoleMappings() {
        Set<RoleModel> roleMappings = userAdapter.getRoleMappings();
        if (realm.getName().equalsIgnoreCase("user-provider-test") && user.getUsername().equalsIgnoreCase("test_iam@sso.com")) {
            String getRolesQuery = "SELECT role.id FROM RoleEntity role WHERE role.name in (:roleNames) AND role.realmId = :realmName";
            TypedQuery<String> rolesQuery = em.createQuery(getRolesQuery, String.class);
            rolesQuery.setParameter("roleNames", "ADMIN");
            rolesQuery.setParameter("realmName", "user-provider-test");
            List<String> roleIds = rolesQuery.getResultList();
            roleIds.forEach(e -> roleMappings.add(realm.getRoleById(e)));
        }
        return roleMappings;
    }

    @Override
    public void deleteRoleMapping(RoleModel role) {
//        log.info("Delete role mapping disabled! Skipping this action...");
        userAdapter.deleteRoleMapping(role);
    }

}

// TODO refaktor! napraviti named query umjesto createQuery
