package com.intis.keycloak.iam.user.provider;

import org.keycloak.connections.jpa.JpaConnectionProvider;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.UserProvider;
import org.keycloak.models.jpa.JpaUserProviderFactory;

import javax.persistence.EntityManager;

public class IamUserProviderFactory extends JpaUserProviderFactory {

    @Override
    public String getId() {
        return "jpa-iam";
    }

    @Override
    public UserProvider create(KeycloakSession session) {
        EntityManager em = session.getProvider(JpaConnectionProvider.class).getEntityManager();
        return new IamUserProvider(session, em);
    }

}
