Adding the custom JPA User Provider to Keycloak
===============================================

Tested on Keycloak 3.4.2.Final.

## Manual configuration

**1. Adding module**

Add files keycloak_config/module.xml and build/libs/keycloak-iam-user-provider-$version.jar to folder
$KEYCLOAK_HOME/modules/com/intis/keycloak/iam/user/provider/main


**2. Changing standalone.xml**

Configure standalone.xml to load this new module and declare the new SPI.

    ...
    
    <subsystem xmlns="urn:jboss:domain:keycloak-server:1.1">
            <web-context>auth</web-context>
            <providers>
                <provider>classpath:${jboss.home.dir}/providers/*</provider>
                <provider>module:com.intis.keycloak.iam.user.provider</provider>
            </providers>
            
    ...

			<spi name="user">
				<default-provider>jpa-iam</default-provider>
				<provider name="jpa" enabled="false" />
				<provider name="jpa-iam" enabled="true" />
			</spi>
			
	...

**3. Run Keycloak**

Run Keycloak. Check if "jpa-iam" user provider was loaded in console output and/or admin web console.


## Docker build

**Build image**

    docker build -t iam/keycloak .

**Run container**

    docker run --rm -d -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -p 8080:8080 --name iam-user-provider iam/keycloak

